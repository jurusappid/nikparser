import provinces from "../data/provinces.js";
import regencies from "../data/regencies.js";
import districts from "../data/districts.js";
import villages from "../data/villages.js";

const getPropinsi = (kode) => {
  return provinces.find(
    (element) => parseInt(element.id) === parseInt(kode)
  );
};

const getKokab = (kode) => {
  return regencies.find(
    (element) => parseInt(element.id) === parseInt(kode)
  );
};

const getKec = (kode) => {
  return districts.find(
    (element) => parseInt(element.id) === parseInt(kode)
  );
};

const getKel = (kode) => {
  const data = [];

  villages.find(
    (o, i) =>{
      if(parseInt(o.district_id) === parseInt(kode)) {
        data.push(villages[i]);
      }
    }
  );

  return data;
}

const nikParser = (nik) => {
  // Output NIK tidak valid
  let res = {
    status: "error",
    message: "NIK tidak valid",
  };

  if (nik.toString().length === 16) {
    const thnNow = new Date().getFullYear().toString().substr(-2);
    // 15 09 12 07 03 93 0001
    const tglLahir = nik.substring(6, 8);
    const blnLahir = nik.substring(8, 10);
    const thnLahir = nik.substring(10, 12);

    let namaProp = "";
    //propinsi
    const cekProp = getPropinsi(nik.substring(0, 2));
    if (cekProp) {
      namaProp = cekProp.name;
    }


    let namaKokab = "";
    // kokab
    const cekKokab = getKokab(nik.substring(0, 4));
    if (cekKokab) {
      namaKokab = cekKokab.name;
    }

    let namaKec = "";
    // kecamatan
    const cekKec = getKec(nik.substring(0, 6));
    if (cekKec) {
      namaKec = cekKec.name;
    }


    let kels = []
    const cekKel = getKel(nik.substring(0, 6));
    if (cekKel) {
      kels = cekKel;
    }

    // jenkel
    let jenKel = "L";
    tglLahir > 40 && (jenKel = "P");

    // tglLahir
    let tgLahir = tglLahir;
    tglLahir > 40 &&
      (tgLahir =
        (parseInt(tglLahir) - 40).toString().length > 0
          ? (parseInt(tglLahir) - 40).toString()
          : `0${(tglLahir - 40).toString()}`);

    // tahun lahir
    let thLahir = `19${thnLahir}`;
    parseInt(thnLahir) < parseInt(thnNow) && (thLahir = `20${thnLahir}`);

    // Output NIK valid
    res = {
      status: "success",
      message: "NIK valid",
      data: {
        nik: nik,
        kelamin: jenKel,
        lahir: `${thLahir}-${blnLahir}-${tgLahir}`,
        provinsi: namaProp,
        kotakab: namaKokab,
        namaKec: namaKec,
        kelurahan: kels,
        uniqcode: nik.substring(12, 16),
      },
    };
  }

  return res;
};

const regionController = {
  nikParser,
}

export default regionController;
