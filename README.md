Parse & Validasi Nomor Induk Kependudukan (NIK) KTP Menggunakan Javascript.
This is revisi from https://github.com/bachors/nik_parse.js.git

<h3>- <a href="#nik_parsejs">region_controller.js</a></h3>
<h3>- <a href="#nik-parse-cli">region_controller cli</a></h3>

---

Data Wilayah

# <a href="https://kodewilayah.id/">kodewilayah.id</a>

Raw CSV

# <a href="https://raw.githubusercontent.com/kodewilayah/permendagri-72-2019/main/dist/base.csv">https://raw.githubusercontent.com/kodewilayah/permendagri-72-2019/main/dist/base.csv</a>

---

Rumus

<img src="kodenik.jpg"/>

# <a href="https://gitlab.com/jurusappid/nikparser/-/blob/master/src/region_controller.js">region_controller.js</a>

Sample

```javascript
import region_controller from "nik-parser-jurusid";

const nik = "1509120703930001";
region_controller(nik);
```

Result

```json
{
  "status": "success",
  "message": "NIK valid",
  "data": {
    "nik": "1509120703010001",
    "kelamin": "L",
    "lahir": "2001-03-07",
    "provinsi": "JAMBI",
    "kotakab": "KAB. TEBO",
    "kecamatan": "Muara Tabir",
    "uniqcode": "0001"
  }
}
```

# <a href="https://www.npmjs.com/package/nik-parse">region_controller cli</a>

Install

```
npm i nik-parser -g
```

Command

```
nik-parser --nik 3204110609970001
```

Alias

```
nik-parser -n 3204110609970001
```

# note

Data yang dihasilkan hanya hasil menterjemahkan tiap digit NIK sehingga data yang dihasilkan adalah tempat pertamakali NIK dibuat/tempat lahir (bukan tempat domisili pemilik NIK secara uptodate).
