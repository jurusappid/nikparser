#!/usr/bin/env node
import fs from "fs";
import fsPromise from "fs/promises"
import { parse } from "csv-parse";

const provinces = "./csv/provinces.csv";
const regencies = "./csv/regencies.csv";
const districts = "./csv/districts.csv";
const villages = "./csv/villages.csv";

const folder = "data";
const data_provinces = [];
const data_regencies = [];
const data_districts = [];
const data_villages = [];

const writeFile = async (data, namaFile) => {
  try {
    if (!fs.existsSync(folder)) {
      fs.mkdirSync(folder);
      console.log(`Folder ${folder} success is created`);
    }

    await fsPromise.writeFile(`./${folder}/${namaFile}`, data, 'utf8');

    console.log(
      `File region is created successfully : ./${folder}/${namaFile}.`
    );
  } catch (err) {
    console.log(err.message);
  }

}

const createProvince = () => {
  fs.createReadStream(provinces)
    .pipe(parse({ delimiter: ";", from_line: 2 }))
    .on("data", function (row) {
      data_provinces.push({
        id: row[0],
        name: row[1],
      });
    }).on("end", async function () {
      let dataDoc = `const provinces = ${JSON.stringify(data_provinces, null, 2)};`;

      dataDoc += `\n\nexport default provinces;`;

      writeFile(dataDoc, "provinces.js");
  }).on("error", function (error) {
    console.log(error.message);
  });
}

const createRegencies = () => {
  fs.createReadStream(regencies)
    .pipe(parse({ delimiter: ";", from_line: 2 }))
    .on("data", function (row) {
      data_regencies.push({
        id: row[0],
        province_id: row[1],
        name: row[2],
      });
    }).on("end", async function () {
    let dataDoc = `const regencies = ${JSON.stringify(data_regencies, null, 2)};`;

    dataDoc += `\n\nexport default regencies;`;


    writeFile(dataDoc, "regencies.js");
  }).on("error", function (error) {
    console.log(error.message);
  });
}

const createDistricts = () => {
  fs.createReadStream(districts)
    .pipe(parse({ delimiter: ";", from_line: 2 }))
    .on("data", function (row) {
      data_districts.push({
        id: row[0],
        regency_id: row[1],
        name: row[2],
      });
    }).on("end", async function () {
    let dataDoc = `const districts = ${JSON.stringify(data_districts, null, 2)};`;

    dataDoc += `\n\nexport default districts;`;

    writeFile(dataDoc, "districts.js");
  }).on("error", function (error) {
    console.log(error.message);
  });
}

const createVillages = () => {
  fs.createReadStream(villages)
    .pipe(parse({ delimiter: ";", from_line: 2 }))
    .on("data", function (row) {
      data_villages.push({
        id: row[0],
        district_id: row[1],
        name: row[2],
      });
    }).on("end", async function () {
    let dataDoc = `const villages = ${JSON.stringify(data_villages, null, 2)};`;

    dataDoc += `\n\nexport default villages;`;

    writeFile(dataDoc, "villages.js");
  }).on("error", function (error) {
    console.log(error.message);
  });
}

createProvince();
createRegencies();
createDistricts();
createVillages();

console.log("successfuly created regions");
