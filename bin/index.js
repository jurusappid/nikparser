#!/usr/bin/env node
import yargs from "yargs";
import regionController from "../src/region_controller.js";

const options = yargs.usage("Usage: --nik <nik>").option("nik", {
  alias: "nik",
  describe: "Nomor Induk Kependudukan",
  type: "string",
}).argv;

const nikParser = (nik) => {
  const res = regionController.nikParser(nik);

  console.log(res);
}

nikParser(options.nik);
